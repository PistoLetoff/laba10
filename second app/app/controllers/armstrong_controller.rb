# frozen_string_literal: true
require 'active_support/all'
require 'net/http'
require 'nokogiri'


class ArmstrongController < ApplicationController
  def input; end

  def result
    uri = URI("http://localhost:3000/armstrong/result.xml?number=#{params[:number]}")
    xml= Net::HTTP.get(uri)
    doc = Nokogiri::XML(xml)
    if params[:format_disp] == 'html'
      xslt = Nokogiri::XSLT(File.read('public/basic_transformer.xslt'))
      @html = xslt.transform(doc).to_s
    else
      pi = Nokogiri::XML::ProcessingInstruction.new(doc, "xml-stylesheet",
                                                    'type="text/xml" href="/client_transformer.xslt"')
      doc.root.add_previous_sibling pi
      render :xml => doc
    end
  end
end
