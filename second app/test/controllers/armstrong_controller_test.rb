# frozen_string_literal: true

require 'test_helper'
require 'open-uri'

class ArmstrongControllerTest < ActionDispatch::IntegrationTest
  test 'should get input' do
    get armstrong_path
    assert_response :redirect
  end

  test 'should get result' do
    get armstrong_input_path
    assert_response :success
  end

  test('should get 548834 for view with 6 HTML') do
    doc = Nokogiri::HTML(open("http://0.0.0.0:3001/armstrong/result?utf8=%E2%9C%93&number=6&format_disp=html&commit=Show+numbers"))
    values = doc.css('td').map &:text
    assert_equal values, %w(0 548834)
  end

  test('should get error for view with 2 HTML') do
    doc = Nokogiri::HTML(open("http://0.0.0.0:3001/armstrong/result?utf8=%E2%9C%93&number=2&format_disp=html&commit=Show+numbers"))
    values = doc.css('.alert').map &:text
    assert_equal values, ["There are't armstrong numbers with 2 digits"]
  end

  test('should get 548834 for view with 4 XML') do
    doc = Nokogiri::XML(open("http://0.0.0.0:3001/armstrong/result?utf8=%E2%9C%93&number=4&format_disp=xmll&commit=Show+numbers"))
    values = doc.xpath('//value').map &:text
    assert_equal values, %w(1634 8208 9474)
  end

  test('should get error for view with -3 XML') do
    doc = Nokogiri::XML(open("http://0.0.0.0:3001/armstrong/result?utf8=%E2%9C%93&number=-3&format_disp=xmll&commit=Show+numbers"))
    values = doc.xpath('//alert').map &:text
    assert_equal values, ["There are't numbers with -3 digits"]
  end
end
