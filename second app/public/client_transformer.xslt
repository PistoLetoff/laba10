<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
    <html>
      <head>
        <title>lr10</title>
      </head>
      <body>
        <xsl:apply-templates select="armstrong"/>
        <a href="/armstrong/input">Go back</a>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="armstrong[count(//value) = 0]">
    <div class="alert alert-danger" role="alert">
      <xsl:apply-templates select="alert"/>
    </div>
  </xsl:template>

  <xsl:template match="armstrong">
    <h1>Armstrong numbers with <xsl:value-of select="//digits"/> digits</h1>
    <table border="1">
      <thead class="thead-dark">
        <th>Item</th>
        <th>Number</th>
      </thead>
      <tbody>
        <xsl:apply-templates select="numbers"/>
      </tbody>
    </table>
  </xsl:template>

  <xsl:template match="numbers">
    <xsl:apply-templates select="number"/>
  </xsl:template>

  <xsl:template match="number">
    <tr>
      <td><xsl:value-of select="index"/></td>
      <td><xsl:value-of select="value"/></td>
    </tr>
  </xsl:template>
</xsl:stylesheet>
