# frozen_string_literal: true
require 'active_support/all'

class ArmstrongController < ApplicationController
  def input; end

  def result
    @n = params[:number].to_i
    @result = []
    alert = ''
    if @n < 1
      alert = "There are't numbers with #{@n} digits"
    elsif @n > 39
      alert = "There are't armstrong numbers with #{@n} digits"
    elsif @n > 7
      alert = "Very big number of digits. My computer will brake =("
    else
      @result = (10**(@n - 1)).upto(10**@n - 1).select do |num|
        num.digits.reduce(0) {|sum, dig| sum + dig ** @n}.equal? num
      end
    end

    if @result.empty? and alert.empty?
      alert = "There are't armstrong numbers with #{@n} digits"
    end

    builder = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
      xml.armstrong do
        xml.numbers do
          @result.each_with_index do |el, i|
              xml.number do
              xml.index i
              xml.value el
            end
          end
        end
        xml.digits @n
        xml.alert alert
      end
    end

    respond_to do |format|
      format.html # index.html.erb
      #format.json { render json: @users }
      format.xml { render :xml => builder }
    end
  end
end
